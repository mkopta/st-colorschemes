st-colorschemes
===============

Colorschemes for terminal emulator st (http://st.suckless.org).

If you have some nice colorschemes to share, please send them.
