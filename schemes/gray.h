const char *colorname[] = {
	"#252525",
	"#ac4142",
	"#90a959",
	"#f4bf75",
	"#6a9fb5",
	"#aa759f",
	"#75b5aa",
	"#d0d0d0",

	"#505050",
	"#ac4142",
	"#90a959",
	"#f4bf75",
	"#6a9fb5",
	"#aa759f",
	"#75b5aa",
	"#f5f5f5",

	[255] = 0,

	"#cccccc",
	"#555555",
};

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 256;
unsigned int defaultrcs = 257;
