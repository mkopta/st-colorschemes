/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#444444", /* black   */
  [1] = "#cf6a4c", /* red     */
  [2] = "#248e0d", /* green   */
  [3] = "#7A6602", /* yellow  */
  [4] = "#5ea6ea", /* blue    */
  [5] = "#9b859d", /* magenta */
  [6] = "#6f849b", /* cyan    */
  [7] = "#E6DA8E", /* white   */

  /* 8 bright colors */
  [8]  = "#444444", /* black   */
  [9]  = "#cf6a4c", /* red     */
  [10] = "#248e0d", /* green   */
  [11] = "#E6DA8E", /* yellow  */
  [12] = "#5ea6ea", /* blue    */
  [13] = "#9b859d", /* magenta */
  [14] = "#6f849b", /* cyan    */
  [15] = "#d9d5ba", /* white   */

  /* special colors */
  [256] = "#d9d5ba", /* background */
  [257] = "#444444", /* foreground */
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor
 */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
static unsigned int defaultcs = 257;
static unsigned int defaultrcs = 256;

/*
 * Colors used, when the specific fg == defaultfg. So in reverse mode this
 * will reverse too. Another logic would only make the simple feature too
 * complex.
 */
static unsigned int defaultitalic = 7;
static unsigned int defaultunderline = 7;
