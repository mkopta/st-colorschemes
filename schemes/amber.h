const char *colorname[] = {
	"#222222",
	"red3",
	"green3",
	"yellow3",
	"#6a9fb5",
	"magenta3",
	"cyan3",
	"#ffa902",

	"gray50",
	"red",
	"green",
	"yellow",
	"#5c5cff",
	"magenta",
	"cyan",
	"white",

	[255] = 0,

	"#ffbf00"
};

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 256;
unsigned int defaultrcs = 257;
