const char *colorname[] = {

    "#2B2B2B",
    "#870000",
    "#5F875F",
    "#875F00",
    "#005FAF",
    "#5F5F87",
    "#008787",
    "#818181",

    "#414141",
    "#D70000",
    "#AFD7AF",
    "#D7AF00",
    "#00AFFF",
    "#AFAFD7",
    "#00D7D7",
    "#CECECE",

	[255] = 0,

	"#cccccc",
};

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 256;
unsigned int defaultrcs = 257;
