const char *colorname[] = {
    "#2d2d2d",
    "#f2777a",
    "#99cc99",
    "#ffcc66",
    "#6699cc",
    "#cc99cc",
    "#66cccc",
    "#d3d0c8",
    "#747369",
    "#f2777a",
    "#99cc99",
    "#ffcc66",
    "#6699cc",
    "#cc99cc",
    "#66cccc",
    "#f2f0ec",

    [255] = 0,

    "#cccccc",
    "#555555",
};

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 256;
unsigned int defaultrcs = 257;
