const char *colorname[] = {
	"#fceec6",
	"red3",
	"#008000",
	"yellow3",
	"blue2",
	"magenta3",
	"cyan3",
	"black",

	"gray50",
	"red",
	"#008000",
	"yellow3",
	"#5c5cff",
	"magenta",
	"cyan",
	"gray30",

	[255] = 0,

	"gray60",
};

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 256;
unsigned int defaultrcs = 257;
