const char *colorname[] = {
	"#402500",
	"#ff9400",
	"#ff9400",
	"#ff9400",
	"#ff9400",
	"#ff9400",
	"#ff9400",
	"#ff9400",

	"#ff9400",
	"#ff9400",
	"#ff9400",
	"#ff9400",
	"#ff9400",
	"#ff9400",
	"#ff9400",
	"#ff9400",

	[255] = 0,

	"#2B1900"
};

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 256;
unsigned int defaultrcs = 257;
