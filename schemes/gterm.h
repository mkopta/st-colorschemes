const char *colorname[] = {
	"black",
	"#CC0000",
	"#4E9A06",
	"#C4A000",
	"#3465A4",
	"#75507B",
	"#06989A",
	"#D3D7CF",

	"#555753",
	"#EF2929",
	"#8AE234",
	"#FCE94F",
	"#729FCF",
	"#AD7FA8",
	"#34E2E2",
	"#EEEEEC",

	[255] = 0,

	"#cccccc",
};

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 256;
unsigned int defaultrcs = 257;
