const char *colorname[] = {
	"#fcfc54",
	"#007878",
	"#008888",
	"#00a8a8",
	"#00b8b8",
	"#bcbc64",
	"#009898",
	"#00a8a8",

	"#00a8a8",
	"#007878",
	"#006600",
	"#00a8a8",
	"#009898",
	"#bcbc54",
	"#009898",
	"#00a8a8",

	[255] = 0,

	"#00a8a8"
};

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
