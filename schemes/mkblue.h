/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#2e3436", /* black   */
  [1] = "#ff7777", /* red     */
  [2] = "#8ae234", /* green   */
  [3] = "#c4a000", /* yellow  */
  [4] = "#729fcf", /* blue    */
  [5] = "#75507b", /* magenta */
  [6] = "#06989a", /* cyan    */
  [7] = "#d3d7cf", /* white   */

  /* 8 bright colors */
  [8]  = "#aaaaaa", /* black   */
  [9]  = "#ff7777", /* red     */
  [10] = "#8ae234", /* green   */
  [11] = "#fce94f", /* yellow  */
  [12] = "#729fcf", /* blue    */
  [13] = "#cd9fc8", /* magenta */
  [14] = "#34e2e2", /* cyan    */
  [15] = "#eeeeec", /* white   */

  /* special colors */
  [256] = "#3a4350", /* background */
  [257] = "#eeeeee", /* foreground */
};

/* very bright yellow eeffbb */

/*
 * Default colors (colorname index)
 * foreground, background, cursor
 */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
unsigned int defaultcs = 257;
unsigned int defaultrcs = 256;

/*
 * Colors used, when the specific fg == defaultfg. So in reverse mode this
 * will reverse too. Another logic would only make the simple feature too
 * complex.
 */
static unsigned int defaultitalic = 7;
static unsigned int defaultunderline = 7;
