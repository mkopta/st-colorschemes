const char *colorname[] = {
	"black",
	"red3",
	"green3",
	"yellow3",
	"#6a9fb5",
	"magenta3",
	"cyan3",
	"gray90",

	"gray50",
	"red",
	"green",
	"yellow",
	"#5c5cff",
	"magenta",
	"cyan",
	"white",

	[255] = 0,

	"#cccccc"
};

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
