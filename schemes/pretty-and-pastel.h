/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#292929", /* black   */
  [1] = "#cf6a4c", /* red     */
  [2] = "#19cb00", /* green   */
  [3] = "#fad07a", /* yellow  */
  [4] = "#8197bf", /* blue    */
  [5] = "#8787af", /* magenta */
  [6] = "#668799", /* cyan    */
  [7] = "#888888", /* white   */

  /* 8 bright colors */
  [8]  = "#525252", /* black   */
  [9]  = "#ff9d80", /* red     */
  [10] = "#23fd00", /* green   */
  [11] = "#ffefbf", /* yellow  */
  [12] = "#accaff", /* blue    */
  [13] = "#c4c4ff", /* magenta */
  [14] = "#80bfaf", /* cyan    */
  [15] = "#e8e8d3", /* white   */

  /* special colors */
  [256] = "#151515", /* background */
  [257] = "#888888", /* foreground */
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor
 */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
unsigned int defaultcs = 257;
unsigned int defaultrcs = 256;

/*
 * Colors used, when the specific fg == defaultfg. So in reverse mode this
 * will reverse too. Another logic would only make the simple feature too
 * complex.
 */
unsigned int defaultitalic = 7;
unsigned int defaultunderline = 7;
